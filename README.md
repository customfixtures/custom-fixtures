Custom Fixtures uses the latest laser cutting and water jet cutting technology, and also provides CNC machining and bending services. We design and construct fabricated metal products, including custom architectural and ornamental metal fabrications, fixtures, and furnishings.

Address: 129 13th Street, Brooklyn, NY 11215, USA
Phone: 718-965-1141
